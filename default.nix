{ kapack ? import
    (fetchTarball "https://github.com/oar-team/nur-kapack/archive/901a5b656f695f2c82d17c091a55db2318ed3f39.tar.gz")
    # commit 02/08/23 "batsim: 4.2.0"
  {}
, doCheck ? false
, doCoverage ? true
, batexpe ? kapack.batexpe
, python ? kapack.pkgs.python3
, pythonPackages ? kapack.pkgs.python3Packages
}:

with kapack.pkgs;

let self = rec {
  batmen = kapack.batsched.overrideAttrs (attr: rec {
    name = "batmen";
    version = "refs/tags/replay_feedback2023";
    src = fetchgit rec {
      url = "https://gitlab.irit.fr/sepia-pub/mael/batmen.git";
      rev = version;
      sha256 = "sha256-YRL+cehPDZP5zqZEhFIuf5DpF8rG5UhLs1oDnuM5U5Q=";
    };
  });

  swf2userSessions = pythonPackages.buildPythonPackage rec {
    pname = "swf2userSessions";
    format = "pyproject";
    version = "refs/tags/replay_feedback2023";
    buildInputs = with pythonPackages; [flit];
    propagatedBuildInputs = [ pythonPackages.networkx ];
    src = fetchgit {
      url = "https://gitlab.irit.fr/sepia-pub/mael/swf2userSessions.git";
      rev = version;
      sha256 = "sha256-Ot+NNpBARn87nqoAOtaD7+zaa+qLvz6RTTSKx6oSP9Q=";
    };
  };

  batmenTools = pythonPackages.buildPythonPackage rec {
    pname = "batmenTools";
    format = "pyproject";
    version = "caefe1c12a059c919d2710ee8a00b9c179faf907"; # commit 26/05/23 "propagate previous change in swf2batsim_split_by_user"
    buildInputs = with pythonPackages; [flit];
    propagatedBuildInputs = [ 
      pythonPackages.pandas
      pythonPackages.numpy 
    ];
    src = builtins.fetchGit {
      url = "https://gitlab.irit.fr/sepia-pub/mael/batmen-tools.git";
      rev = version;
    };
  };

  exp_env = mkShell rec {
    buildInputs = with pythonPackages; [
      kapack.batsim-420
      batmen
      swf2userSessions
      batmenTools
      batexpe
      kapack.evalys       # for data visualization
      ipykernel jupyter   # for jupyter notebood
      pandas numpy matplotlib jinja2 pip  # for data analysis
      hostname curl
      # wget
    ];
  };

  exp_custom_batmen = mkShell rec {
    shellHook = ''
      export PATH="../batmen/build:$PATH"
    '';
    buildInputs = [
      batsim_latest
      batexpe
    ];
  };
};
in
  self


